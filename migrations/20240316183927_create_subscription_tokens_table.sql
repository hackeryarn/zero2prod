create table subscription_tokens(
    subscription_token text not null,
    subscriber_id uuid not null
        REFERENCES subscriptions (id),
    primary key (subscription_token)
)
