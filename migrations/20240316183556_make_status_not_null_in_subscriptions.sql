begin;
    update subscriptions
        set status = 'confirmed'
        where status IS NULL;
    alter table subscriptions alter column status set not null;
commit;
