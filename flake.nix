{
  inputs = {
    nixpkgs.url = "github:nixos/nixpkgs/nixos-unstable";
    flake-utils.url = "github:numtide/flake-utils";
  };
  outputs =
    {
      self,
      nixpkgs,
      flake-utils,
    }:
    flake-utils.lib.eachDefaultSystem (
      system:
      let
        pkgs = import nixpkgs { inherit system; };
        buildInputs = with pkgs; [ openssl ];
        nativeBuildInputs = with pkgs; [
          bunyan-rs
          cargo
          cargo-audit
          cargo-tarpaulin
          cargo-udeps
          cargo-watch
          clippy
          pkg-config
          postgresql_16
          rust-analyzer
          rustc
          rustfmt
          sqlx-cli
        ];
      in
      with pkgs;
      {
        devShells.default = mkShell {
          inherit buildInputs nativeBuildInputs;
          RUST_BACKTRACE = 0;
          RUST_SRC_PATH = "${pkgs.rust.packages.stable.rustPlatform.rustLibSrc}";
        };
      }
    );
}
